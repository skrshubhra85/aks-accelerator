# 07 - Using Gitlab CI/CD to build an Image

## 7.0 Introduction
In this section we will use Gitlab CI/CD to build our code, then our container image.

## 7.1 Defining CI/CD Stages

Gitlab's Pipelines are _Pipelines as Code_, which is a modern approach to Continuous Delivery.

To get started, we create a `.gitlab-ci.yml` file in our project's root.

Initially, we want to:

- Build our software
- Create our container image and push it to our registry

In Gitlab terms, we will model these two requirements as separates _stages_:

```
stages:
  - build
  - push
```


## 7.2 The Build Code Job

### 7.2.1 Building a Jar With Gradle
Continuing our Java example (.NET or other languages would require something equivalent), we can do the following, based on the official [Gitlab Gradle sample](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/lib/gitlab/ci/templates/Gradle.gitlab-ci.yml) (note that Gitlab maintain many official samples - see if one is available for your language).

```
image: gradle:alpine

before_script:
  - export GRADLE_USER_HOME=${CI_PROJECT_DIR}/.gradle

build_code:
  stage: build
  environment:
    name: staging
  script:
    - gradle --build-cache assemble
  cache:
    key: "$CI_COMMIT_REF_NAME"
    policy: push
    paths:
      - build
      - .gradle

```

### 7.2.1 Building the Jar
This configuration will:

- set our `$GRADLE_USER_HOME` by **exporting** an environment variable
- declare a `build_code` job as part of the `build` stage in our Pipeline
- use the [Gradle Alpine](https://hub.docker.com/_/gradle?tab=description) **image**
- build our code using a `gradle assemble` **script** (a [lifecycle job](https://docs.gradle.org/current/userguide/base_plugin.html#sec:base_jobs) that will produce a jar))
- configure our job for the `staging` **environment** "Environment Scope" (recall that we told Gitlab that this was the purpose of our Kubernetes cluster)
- **cache**, using the $COMMIT_REF_NAME as a key, the `build` and `.gradle` paths (to speed up subsequent builds - so that e.g. dependencies don't need to be re-downloaded). See [Gitlab's documentation on caching](https://docs.gitlab.com/ee/ci/caching/)

### 7.2.2 Making the Jar Available to the next stage of the pipeline

Since we now want to do something else - copy our Jar into a container image - we must make sure we pass this built artefact through to the next stage of our pipeline.

We do this with the `artefacts` object, simply declaring what we want to make available, and for how long we wish to keep it.

```
  artifacts:
    paths:
      - build
    expire_in: 1 hour
```

> Note: this mechanism is not intended to be used for permanent artefact storage.

## 7.2 The Build Image Job

With our Jar created, we can now automate the creation & push of our container images.

Since Gitlab is already executing our jobs in containers, we must use "Docker in Docker" in order to build & push. See the [Gitlab Docker Executor documentation](https://docs.gitlab.com/runner/executors/docker.html) for more details.

We declare a new job, `build_image`, under stage `push`, with environment `staging` as before.

Since we are running docker in docker ("dind"), we use image `docker:stable`.

```
build_image:
  stage: push
  environment:
    name: staging
  image:
    name: docker:stable
```

We mark it as being dependent on the previous job, `build_code`, which mean we can consume the `artefact` that we output:

```
  dependencies:
    - build_code
```

Finally, we run our now familiar docker commands:

```
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build -t registry.gitlab.com/citihub/aks-accelerator:$CI_COMMIT_SHORT_SHA --build-arg PROJECT_DIR=${CI_PROJECT_DIR} .
    - docker push registry.gitlab.com/citihub/aks-accelerator:$CI_COMMIT_SHORT_SHA
```

- We pass the built in environment variable `$CI_JOB_TOKEN` to authenticate the built-in `gitlab-ci-token` user
- Similarly we use `$CI_REGISTRY` for the location of our project's container registry
- To ensure that the images we push have different tags, we refer to `$CI_COMMIT_SHORT_SHA`

> Note: The latter becomes important when we want to continuously deploy to Kubernetes - we need to have a way of asking Kubernetes to update to a newer, tagged image

At [Gitlab's recommendation](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html), we specify the following:

```
variables:
   # Note that if you're using Kubernetes executor, the variable should be set to
   # tcp://localhost:2375 because of how Kubernetes executor connects services
   # to the job container
   DOCKER_HOST: tcp://localhost:2375/
   # When using dind, it's wise to use the overlayfs driver for
   # improved performance.
   DOCKER_DRIVER: overlay2

services:
  - docker:dind
```

> Note: `DOCKER_DRIVER` indicates the storage driver we would like to use. "Overlay" refers to `OverlayFS`, a union filesystem used by Docker which is more efficient for our use case here. `overlay2` is [the more up-to-date driver](https://docs.docker.com/storage/storagedriver/overlayfs-driver/) for OverlayFS.

## 7.3 Viewing our Pipeline

If we commit code to our pipeline, we can now watch it build:

![Gitlab Pipeline](./img/07_Pipeline.png "Gitlab Pipeline")

And within the pipeline, we can see our Build Code job:

![Gitlab Build Code Job](./img/07_Build_Code_Job.png "Gitlab Build Code Job")

Notice that:

- the Job is part of a Pipeline
- in this particular screenshot, the artefact has expired and been removed automatically


## 7.4 Viewing our Images

We can also see our images in the Gitlab UI:

![Gitlab Container Registry](./img/07_container_registry.png "Gitlab Container Registry")

## Next
In the next section we will use Gitlab CI/CD to deploy our fresh container image to AKS.

< [07 Building a Container Image with Gitlab CI/CD](06_deploying_to_aks.md) | 7 Building a Container Image with Gitlab CI/CD | [08 Deploying a Container Image with Gitlab CI/CD](08_gitlab_ci_deploy_image.md) >